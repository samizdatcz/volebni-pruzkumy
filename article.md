---
title: "Data: Jak se v Česku mýlí volební průzkumy – a proč je přesto nezavrhovat"
perex: "Český rozhlas porovnal data z předvolebních průzkumů CVVM se skutečnými volebními výsledky. Které strany výzkum v průběhu posledních patnácti let příliš favorizoval a které podcenil?"
description: "Český rozhlas porovnal data z předvolebních průzkumů CVVM se skutečnými volebními výsledky. Které strany výzkum v průběhu posledních patnácti let příliš favorizoval a které podcenil?"
authors: ["Jan Cibulka", "Michal Zlatkovský"]
published: "13. února 2017"
# coverimg: https://interaktivni.rozhlas.cz/volebni-pruzkumy/media/social.png
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "volebni-pruzkumy"
socialimg: https://interaktivni.rozhlas.cz/volebni-pruzkumy/media/social.jpg
libraries: [d3, jquery, "https://interaktivni.rozhlas.cz/tools/tooltip/v1.1.5.js"]
styles: ["./styl/chart.css", "https://interaktivni.rozhlas.cz/tools/tooltip/v1.1.5.css"]
recommended:
  - link: https://interaktivni.rozhlas.cz/horko-ve-mestech/
    title: Mapa pražského horka: Stromy ochladí okolí o několik stupňů
    perex: Hlavní město hledá cesty, jak se vypořádat s tropickými vedry.
    image: https://interaktivni.rozhlas.cz/horko-ve-mestech/media/socimg.png
---

<div data-bso="1"></div>

V souvislosti s americkými prezidentskými volbami, v nichž překvapivě zvítězil Donald Trump, a hlasováním o vystoupení Británie z EU se čím dál častěji ozývají hlasy zpochybňující předvolební průzkumy. Jsou pravidelná zjišťování volebních preferencí spolehlivým ukazatelem skutečných volebních výsledků, nebo v odhadech selhávají?

Že výsledek voleb předpovědět nelze, je jasné i samotným výzkumníkům. Jedno z českých výzkumných pracovišť, [Centrum pro výzkum veřejného mínění](http://cvvm.soc.cas.cz/) (CVVM), u všech výsledků průzkumů uvádí, že <i>stranické preference představují aktuální rozložení sympatií k politickým stranám v celé společnosti a nelze je zaměňovat s volební prognózou.</i> Přesto média i jejich čtenáři většinou považují výsledky průzkumů za směrodatné. V případě českých sněmovních voleb se ale průzkumy minuly s volebními výsledky hned několikrát. Ukazují to [volební modely CVVM](http://volebnipruzkumy.cz/), které Český rozhlas jako výstup veřejné výzkumné instituce zpracoval.

<aside class="big">
  <div class="chart cssd"></div>
</aside>

Hned u dvou předešlých parlamentních voleb průzkumy přisoudily příliš velkou podporu sociální demokracii. "CVVM dlouhodobě dotazuje preference spontánní otázkou - přijdou za dotazovaným a zeptají se 'koho budete volit?', on pak musí v deseti sekundách odpovědět. To vede k nadhodnocování velkých stran," [nabízí možné vysvětlení](http://www.rozhlas.cz/plus/proaproti/_porad/101030) sociolog z agentury Median Daniel Prokop. "V době, kdy u CVVM vycházela hodně dobře ČSSD, byla ČSSD hlavní opoziční strana - první, na kterou si vzpomenete, když nemáte rád vládu."

<aside class="big">
  <div class="chart ano"></div>
</aside>

Popularita dnešní druhé nejsilnější parlamentní strany, ANO, strmě vzrostla až těsně před parlamentními volbami v roce 2013, proto ji průzkumy nestihly zohlednit. Dnes ale podle Prokopa získalo ANO ve volebních modelech roli ČSSD. "Je stranou, která je přijatelná pro velkou část populace, je mediálně nejviditelnější. Když zvažujete více stran a někdo se vás 'z voleje' zeptá, často řeknete ANO. Převzalo úlohu strany, která dopadá dobře ve spontánní otázce, proto je u CVVM vysoko."

<aside class="big">
  <div class="chart kdu-csl"></div>
</aside>

Výzkumy mohou také zpětně ovlivnit voliče - zvlášť při špatné interpretaci. "Před volbami v roce 2010 vycházely výzkumy, které přisoudily lidovcům ve stranických preferencích - tedy včetně lidí, kteří říkají <i>nevím</i> - 4,5 procenta," říká Prokop. "Tenhle výzkum, který vůbec neodhaduje výsledky voleb, se medializoval - a to stranu může poškodit." Teprve na základě stranických preferencí se však sestavuje klíčový volební model, který ukazuje, jak by byly hlasy voličů rozvrženy v pomyslných volbách. Situace se od té doby podle Prokopa zlepšila - přesto jsou průzkumy stále často považovány za předpověď výsledků voleb.

<aside class="big">
  <div class="chart kscm"></div>
</aside>

Průzkumy nejsou dokonalé, ale častěji než v jejich provedení se chybuje v jejich čtení. Volební modely totiž neznamenají jistotu, ale pravděpodobnost. V případě amerických prezidentských voleb dával slavný statistik Nate Silver Donaldu Trumpovi [šanci 28,6 procent](https://projects.fivethirtyeight.com/2016-election-forecast/). To, že Trump nakonec vyhrál, ale Silverův odhad neznehodnocuje: jen nastala méně pravděpodobná, ale stále velmi reálná varianta. Ve [své eseji](http://neovlivni.cz/esej-michala-skopa-pruzkumy-se-nepletou-jejich-vykladaci-ano/) to komentuje spoluzakladatel projektu [KohoVolit.eu](http://kohovolit.eu/) Michal Škop: "Některé předpovědi počasí také neuvádějí pouze to, jestli bude pršet, ale rovnou pravděpodobnost srážek. Pokud byste v takové předpovědi slyšeli, že bude pršet s pravděpodobností třeba 25&nbsp;%, a skutečně by zapršelo, mysleli byste si, že se meteorologové spletli? Nejspíše ne."

<aside class="big">
  <div class="chart top-09"></div>
</aside>

I v samotných průzkumech se počítá s takzvanou statistickou chybou. Ta je způsobena tím, že se výzkumnicí ptají jen malého vzorku obyvatel, v případě CVVM kolem tisícovky lidí, a pohybuje se kolem tří procent. Kromě toho ale průzkumy chybují ze své podstaty. "Nikdo, žádná firma pro výzkum veřejného mínění, totiž není schopná udělat průzkum naprosto perfektně," podotýká Škop.

<aside class="big">
  <div class="chart ods"></div>
</aside>

Takové "chyby z podstaty" vznikají kvůli chování dotázaných, které nelze nikdy s jistotu předvídat. "Někomu zavolají a on se s nimi nechce bavit, protože má práci. Nebo dotazovaný nechce přiznat, že bude volit Trumpa (nebo v ČR komunisty). Nebo na něj dotazující firma nemá telefon. Nebo jim neotevře při osobním dotazování, protože už je starší a cizím lidem neotevírá. Nebo se na poslední chvíli rozhodne, že nakonec k těm volbám přeci jen půjde, i když předtím tazatelům řekl, že ne," vypočítává ve svém textu Škop a reálnou chybu v průzkumech odhaduje zhruba na pět procent. To však volební modely neznehodnocuje - jen je při jejich čtení potřeba mít na paměti, že je výzkumné agentury nepřipravují jako bezchybné prognózy. Ty v oblasti předvolebních průzkumů skutečně neexistují.
