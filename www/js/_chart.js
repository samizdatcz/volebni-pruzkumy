var cze = d3.locale({
  "decimal": ".",
  "thousands": ",",
  "grouping": [3],
  "currency": ["$", ""],
  "dateTime": "%a %b %e %X %Y",
  "date": "%m/%d/%Y",
  "time": "%H:%M:%S",
  "periods": ["AM", "PM"],
  "days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
  "shortDays": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
  "months": ["lednu", "únoru", "březnu", "dubnu", "květnu", "červnu", "červenci", "srpnu", "září", "říjnu", "listopadu", "prosinci"],
  "shortMonths": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
});

d3.time.format = cze.timeFormat;

var partyNamesArr = [{"id":"ano","name":"Hnut\u00ed ANO","abbreviation":"ANO"},{"id":"cssd","name":"\u010cesk\u00e1 strana soci\u00e1ln\u011b demokratick\u00e1","abbreviation":"\u010cSSD"},{"id":"top-09","name":"TOP 09","abbreviation":"TOP 09"},{"id":"ods","name":"Ob\u010dansk\u00e1 demokratick\u00e1 strana","abbreviation":"ODS"},{"id":"kdu-csl","name":"K\u0159es\u0165ansko demokratick\u00e1 uni-\u010ceskoslovensk\u00e1 strana lidov\u00e1","abbreviation":"KDU-\u010cSL"},{"id":"usvit","name":"\u00dasvit p\u0159\u00edm\u00e9 demokracie","abbreviation":"\u00dasvit"},{"id":"zeleni","name":"Strana zelen\u00fdch","abbreviation":"Zelen\u00ed"},{"id":"pirati","name":"\u010cesk\u00e1 pir\u00e1tsk\u00e1 strana","abbreviation":"Pir\u00e1ti"},{"id":"svobodni","name":"Strana svobodn\u00fdch ob\u010dan\u016f","abbreviation":"Svobodn\u00ed"},{"id":"nezavisli-kandidati","name":"Nez\u00e1visl\u00ed kandid\u00e1ti","abbreviation":"Nez\u00e1visl\u00ed kandid\u00e1ti"},{"id":"jine-strany","name":"Jin\u00e9 strany","abbreviation":"Jin\u00e9"},{"id":"ostatni-strany","name":"Ostatn\u00ed strany","abbreviation":"Ostatn\u00ed"},{"id":"zadna","name":"Nev\u00ed, \u017e\u00e1dn\u00e1 strana","abbreviation":"\u017e\u00e1dn\u00e1"},{"id":"nevolic","name":"Nep\u016fjde volit","abbreviation":"nevoli\u010d"},{"id":"rozhodne-ano","name":"rozhodn\u011b ano","abbreviation":"rozhodn\u011b ano"},{"id":"spise-ano","name":"sp\u00ed\u0161e ano","abbreviation":"sp\u00ed\u0161e ano"},{"id":"spise-ne","name":"sp\u00ed\u0161e ne","abbreviation":"sp\u00ed\u0161e ne"},{"id":"rozhodne-ne","name":"rozhodn\u011b ne","abbreviation":"rozhodn\u011b ne"},{"id":"nevi","name":"nev\u00ed","abbreviation":"nev\u00ed"},{"id":"urcite-ano","name":"ur\u010dit\u011b ano","abbreviation":"ur\u010dit\u011b ani"},{"id":"urcite-ne","name":"ur\u010dit\u011b ne","abbreviation":"ur\u010dit\u011b ne"},{"id":"kscm","name":"Komunistick\u00e1 strana \u010cech a Moravy","abbreviation":"KS\u010cM"},{"id":"vv","name":"V\u011bci ve\u0159ejn\u00e9","abbreviation":"VV"},{"id":"ns-lev-21","name":"N\u00e1rodn\u00ed socialist\u00e9-levice 21. stolet\u00ed","abbreviation":"LEV 21"},{"id":"spo","name":"Strana pr\u00e1v ob\u010dan\u016f","abbreviation":"SPO"},{"id":"lidem","name":"LIDEM-liber\u00e1ln\u00ed demokrat\u00e9","abbreviation":"LIDEM"},{"id":"cps","name":"\u010cesk\u00e1 pir\u00e1tsk\u00e1 strana","abbreviation":"\u010cPS"},{"id":"ds","name":"D\u011blnick\u00e1 strana","abbreviation":"DS"},{"id":"snk-ed","name":"SNK Evrop\u0161t\u00ed demokrat\u00e9","abbreviation":"SNK ED"},{"id":"szj","name":"Strana za \u017eivotn\u00ed jistoty","abbreviation":"S\u017dJ"},{"id":"spoz","name":"Strana pr\u00e1v ob\u010dan\u016f Zemanovci","abbreviation":"SPOZ"},{"id":"rms","name":"Republik\u00e1ni Miroslava Sl\u00e1dka","abbreviation":"RMS"},{"id":"lsns","name":"Svobodn\u00ed demokrat\u00e9-LSNS","abbreviation":"LSNS"},{"id":"pravy-blok","name":"Prav\u00fd blok","abbreviation":"Prav\u00fd blok"},{"id":"snk","name":"SNK sdru\u017een\u00ed nez\u00e1visl\u00fdch","abbreviation":"SNK"},{"id":"evropsti-demokrate","name":"Evrop\u0161t\u00ed demokrat\u00e9","abbreviation":"Evrop\u0161t\u00ed demokrat\u00e9"},{"id":"nezavisli","name":"NEZ\u00c1VISL\u00cd","abbreviation":"NEZ\u00c1VISL\u00cd"},{"id":"ctyrkoalice","name":"\u010dty\u0159koalice","abbreviation":"\u010dty\u0159koalice"},{"id":"us-deu","name":"Unie svobody-Demokratick\u00e1 unie","abbreviation":"US-DEU"},{"id":"oda","name":"Ob\u010dansk\u00e1 demokratick\u00e1 aliance","abbreviation":"ODA"},{"id":"csns","name":"\u010cesk\u00e1 strana n\u00e1rodn\u011b soci\u00e1ln\u00ed","abbreviation":"\u010cSNS"},{"id":"szr","name":"Strana zdrav\u00e9ho rozumu","abbreviation":"SZR"},{"id":"spr-rsc","name":"Sdru\u017een\u00ed pro republiku-Republik\u00e1nsk\u00e1 strana \u010ceskoslovenska","abbreviation":"SPR-RS\u010c"},{"id":"ns","name":"N\u00e1rodn\u00ed strana","abbreviation":"NS"},{"id":"koalice","name":"Koalice","abbreviation":"Koalice"},{"id":"nadeje","name":"Nad\u011bje","abbreviation":"Nad\u011bje"},{"id":"mns-hss","name":"Mor.n\u00e1r.str.-Hn.slezskom.sjed.","abbreviation":"MNS-HSS"},{"id":"dzj","name":"D\u016fchodci za \u017eivotn\u00ed jistoty","abbreviation":"D\u017dJ"},{"id":"sdl","name":"Strana demokratick\u00e9 levice","abbreviation":"SDL"},{"id":"cus","name":"\u010ceskomoravsk\u00e1 unie st\u0159edu","abbreviation":"\u010cUS"},{"id":"cp","name":"\u010cesk\u00e1 pravice","abbreviation":"\u010cP"},{"id":"lb","name":"Lev\u00fd blok","abbreviation":"LB"},{"id":"hsms-mns","name":"Hn.samosp.M.aSl.-Mor.n\u00e1r.sjed.","abbreviation":"HSMS-MNS"},{"id":"ok-pk","name":"Ob\u010dansk\u00e1 koalice-Politick\u00fd klub","abbreviation":"OK-PK"},{"id":"mds","name":"Moravsk\u00e1 demokratick\u00e1 strana","abbreviation":"MDS"},{"id":"nds","name":"N\u00e1rodn\u011b demokratick\u00e1 strana","abbreviation":"NDS"},{"id":"dl","name":"Demokratick\u00e1 liga","abbreviation":"DL"},{"id":"bpo","name":"Balb\u00ednova poetick\u00e1 strana","abbreviation":"BPO"},{"id":"vb","name":"Volba pro budoucnost","abbreviation":"VB"},{"id":"ha","name":"Humanistick\u00e1 aliance","abbreviation":"HA"},{"id":"akce-za-zruseni-senatu","name":"Akce za zru\u0161en\u00ed sen\u00e1tu a proti vytunelov\u00e1n\u00ed d\u016fchodov\u00fdch fond\u016f","abbreviation":"AZS"},{"id":"cz","name":"Cesta zm\u011bny","abbreviation":"CZ"},{"id":"romska-strana","name":"Romsk\u00e1 ob\u010dansk\u00e1 iniciativa \u010cR","abbreviation":"RS"},{"id":"strana-venkova","name":"Strana venkova a spojen\u00e9 ob\u010dansk\u00e9 s\u00edly","abbreviation":"SV"},{"id":"csdh","name":"\u010cesk\u00e9 soci\u00e1ln\u011b demokratick\u00e9 hnut\u00ed","abbreviation":"\u010cSDH"},{"id":"koalice-kdu-csl-us-deu","name":"Koalice KDU-\u010cSL, US-DEU","abbreviation":"Koalice KDU-\u010cSL,US-DEU"},{"id":"ps","name":"Pr\u00e1vo a Spravedlnost","abbreviation":"PS"},{"id":"koruna-ceska","name":"Koruna \u010desk\u00e1-monarchistick\u00e1 strana","abbreviation":"K\u010c"},{"id":"helax","name":"Helax-Ostrava se bav\u00ed","abbreviation":"Helax"},{"id":"4-vize","name":"4 VIZE-www.4vize.cz","abbreviation":"4 VIZE"},{"id":"moravane","name":"Moravan\u00e9","abbreviation":"Moravan\u00e9"},{"id":"hs","name":"Humanistick\u00e1 strana","abbreviation":"HS"},{"id":"koalice-cr","name":"Koalice pro \u010ceskou republiku","abbreviation":"Koalice \u010cR"},{"id":"ns","name":"N\u00e1rodn\u00ed strana","abbreviation":"NS"},{"id":"fis","name":"Folklor i Spole\u010dnost","abbreviation":"FiS"},{"id":"srs","name":"STRANA ROVNOST \u0160ANC\u00cd","abbreviation":"SR\u0160"},{"id":"obcane-cz","name":"OB\u010cAN\u00c9.CZ","abbreviation":"OB\u010cAN\u00c9.CZ"},{"id":"ks","name":"Konzervativn\u00ed strana","abbreviation":"KS"},{"id":"stop","name":"STOP","abbreviation":"STOP"},{"id":"dsss","name":"D\u011blnick\u00e1 strana soci\u00e1ln\u00ed spravedlnosti","abbreviation":"DSSS"},{"id":"kh","name":"Kl\u00ed\u010dov\u00e9 hnut\u00ed","abbreviation":"KH"},{"id":"hlavu-vzuru","name":"HLAVU VZH\u016eRU-volebn\u00ed blok","abbreviation":"HLAVU VZH\u016eRU"},{"id":"zmena","name":"Politick\u00e9 hnut\u00ed ZM\u011aNA","abbreviation":"ZM\u011aNA"},{"id":"soukromnici","name":"Strana soukromn\u00edk\u016f \u010cR","abbreviation":"Soukromn\u00edci"},{"id":"aktiv-nezavislych-obcanu","name":"Aktiv nez\u00e1visl\u00fdch ob\u010dan\u016f","abbreviation":"Aktiv nez\u00e1visl\u00fdch ob\u010dan\u016f"},{"id":"suverenita","name":"SUVERENITA","abbreviation":"Suverenita"},{"id":"spd","name":"Svoboda a p\u0159\u00edm\u00e1 demokracie","abbreviation":"SPD"}]

var partyNames = {};

for (var i in partyNamesArr) {
  partyNames[partyNamesArr[i]['id']] = partyNamesArr[i]['abbreviation']
};

var wi = window.innerWidth;

if (wi > 1000) {
  wi = 1000;
};

  var margin = {top: 20, right: 15, bottom: 60, left: 60}
      , width = wi - margin.left - margin.right
      , height = (wi * 0.45) - margin.top - margin.bottom;

  if (height < 200) {
    height = 200
  };
  
  var y = d3.scale.linear()
          .domain([0, 48])
          .range([ height, 0 ]);

  var x = d3.time.scale()
          .domain([new Date('2002-01-28'), new Date('2015-12-31')])
          .range([0, width]);

function cvvmDate(val) {

    try {
      var format = d3.time.format("%Y-%m");
      var newFormat = d3.time.format("%B %Y");
      var dt = format.parse(val);
      return newFormat(dt);
    } catch(err) {
      return val;
    }   
  };

  function makeTooltip(evt) {
    var txt = '';
    if (evt.pollster_id == 'volby') {
        txt = txt + 'Ve <b>volbách ';
      } else {
        txt = txt + 'Ve volebním modelu CVVM v <b>';
      };

      txt = txt + cvvmDate(evt.poll_identifier) + '</b> získala strana <b>' + partyNames[evt.choice_id] + ' ' 
        + Math.round(evt.value * 1000 ) / 10
        + ' %</b> hlasů.'

      return txt
  };

function drawChart(party) {
  var chart = d3.select('.' + party)
    .append('svg:svg')
    .attr('width', width + margin.right + margin.left)
    .attr('height', height + margin.top + margin.bottom)
    .attr('class', 'chart')

  var main = chart.append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
    .attr('width', width)
    .attr('height', height)
    .attr('class', 'main')   
      
  var xAxis = d3.svg.axis()
    .scale(x)
    .ticks(d3.time.years.utc, 1)
    .tickFormat(d3.time.format.utc('%Y'))
    .orient('bottom');

  main.append('g')
    .attr('transform', 'translate(10,' + (height + 10) + ')')
    .attr('class', 'main axis date')
    .call(xAxis)
    .selectAll("text")
    .attr("transform", function(d) {
      return "rotate(-65)" 
    });

  var yAxis = d3.svg.axis()
    .tickSize(0)
    .scale(y)
    .orient('left');

  main.append('g')
    .attr('transform', 'translate(-15, 0)')
    .attr('class', 'main axis date')
    .call(yAxis);

  main.append('text')
    .attr('x',(width)/2)
    .attr('text-anchor','middle')
    .attr('style','font-size: 130%')
    .text('Volební modely: ' + partyNames[party]);
 
  var g = main.append("svg:g");

  new Tooltip().watchElements()

    d3.csv("./data/data.csv", function(data) {
      
      function filterResults(val) {
        return val.choice_id == party;
      };

      data = data.filter(filterResults);

      var dots = g.selectAll("scatter-dots")
      .data(data)
      .enter().append("svg:circle")
          .attr("cx", function (d,i) {
            return x(new Date(d.end_date)); 
          } )
          .attr("cy", function (d) { 
            return y(parseFloat(d.value) * 100); 
          } )
          .attr('r', function (d) {
            if (d.pollster_id == 'volby') {
              return 4;
            } else {
              return 3;
            }
          })
          .attr('fill', function (d) {
            if (d.pollster_id != 'volby') {
              return '#a6cee3';
            } else {
              return '#e31a1c';
            }
          })
          .style("opacity", 0.7)
          .attr("data-tooltip", function(d) {
            return makeTooltip(d);
      });
    });
  };

$(".chart").each(function(index) {
  drawChart($(this).attr('class').split(' ')[1]);
});